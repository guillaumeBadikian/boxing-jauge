# boxing-jauge

## Présentation générale
Le sujet de base est simple : il faut créer une API permettant la gestion des réservations pour une salle de boxe Thaï.
Dans un contexte COVID, la salle ne peut recevoir autant de public que souhaité.
Officiellement la jauge est de 25 personnes maximum (modifiable).
La salle dispose 30 pairs de Pao, 30 cordes à sauter et 18 sacs de frappe.

Notre salle dispense 2 cours par jour. Un le midi, un le soir tous les jours de la semaine.

Le midi, la salle organise un cours de cardio-boxing qui est ouvert à tous, moyennant une réservation au préalable.
Ce cours se réalise sur l'espace ouvert de la salle et ne nécessite que des cordes à sauter. Tout le reste se fait au poids du corps.

Le soir, la salle organise un cours de boxe thaï ouvert à tous, moyennant une réservation au préalable.
Ce cours se déroule autour des sacs de frappe uniquement. Un adhérent par sac, pas plus.
Restrictions sanitaires oblige, pas de sparring, ni de binôme avec des Paos.

### Fonctions
Nous allons devoir mettre en place une API simple sans authentification ni autorisation afin que des applications native Android et iOS puissent s'y connecter.
Un futur plug-in Wordpress permettant de s'y connecter sera réalisé ensuite.

Notre API doit :
- Obtenir la jauge en pourcentage pour chaque cours (Permet de connaître si un cours est complet ou non)
- Réserver un cours (Restons simples, un simple nom + prénom est acceptable pour le moment)
- Obtenir la liste des participants pour un jour donné + midi ou soir 

## Les Pré-requis
PHP >= 7.1
Symfony >= 4
On attend aussi de vous que le code soit testable et testé.
Pas de test unitaire.
Il est conseillé de finir les points requis avant de s'attaquer au bonus.
Il est aussi conseillé de faire un maximum de commit pour bien détailler les étapes de votre raisonnement au cours du développement.
Le temps est libre mais il est tout de même conseillé de passer moins de 1h30 sur le sujet (temps de setup d'environnement compris)

## Libertés
Le choix de votre environnement PHP ainsi que du serveur d'application associé reste à votre discrétion
Le moyen de stockage des données reste à votre discrétion
Vous pouvez utiliser tout bundle ou composants Sf qui pourrait vous être utile

## Bonus
Si le temps le permet :
- Une authentification avec un login (email) + mot de passe pour récupérer un Bearer/Token
- Une gestion des rôles pour autoriser des actions spécifiques, les autres sont réputées publique moyennant authentification
- Réservation / Modification / Annulation d'un créneau pour un adhérent de la salle
- Register avec comme pré-requis un email (servant de login) + un mot de passe + un Nom et prénom + un sexe (pour la mise en place de cours réservés aux femmes) + date de naissance
- Annuler un cours entier (En cas de détection d'un cas de COVID), doit simuler un envoi de mail aux participants.
